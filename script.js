// original: https://codepen.io/blink172/pen/vERyxK

var click = document.getElementById('click');
var textarea = document.getElementById('textarea');
var clicks = document.getElementById('count');
var dcCount = document.getElementById('dcCount');
var reset = document.getElementById('reset');
reset.onclick = function () {
    clicks.innerHTML = 0;
    dcCount.innerHTML = 0;
    textarea.value = '';
    click.style.background = '#10B981';
    reset.style.color = '#047857';
};

var prevClickMicrotime = microtime(true);
function microtime(get_as_float) {
    var now = new Date().getTime() / 1000;
    var s = parseInt(now, 10);

    return get_as_float ? now : Math.round((now - s) * 1000) / 1000 + ' ' + s;
}

function clickEvent() {
    clickTime = microtime(true);
    diff = clickTime - prevClickMicrotime;
    if (diff <= 0.08) {
        click.style.background = '#EF4444';
        reset.style.color = '#DC2626';
        dcCount.innerHTML++;
    }
    textarea.value = diff + '\n' + textarea.value;
    prevClickMicrotime = clickTime;
    clicks.innerHTML++;
}

function mouseClick() {
    var e = window.event;
    console.log(e);
    clickEvent();
    return false;
}
