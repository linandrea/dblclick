# dblclick
Mouse double-click tester.

Test if your mouse double clicks.

## Info
This project is a recolor of [blink192's CodePen](https://codepen.io/blink172) [double click mouse test page](https://codepen.io/blink172/pen/vERyxK).
